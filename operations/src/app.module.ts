import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ZerodhaModule } from './zerodha/zerodha.module';

@Module({
  imports: [ZerodhaModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
