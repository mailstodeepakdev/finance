import { Controller, Get } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}
  @MessagePattern('buy')
  async reciveStocks(data: object) {
    console.log('OPERATIONS::');
    console.log(data);
    return true;
  }
}
