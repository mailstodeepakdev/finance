import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MainModule } from './main/main.module';
import { KafkaModule } from './kafka/kafka.module';

@Module({
  imports: [MainModule, KafkaModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
