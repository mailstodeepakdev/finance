export interface Stock {
  stockName: string;
  price: number;
}
