import {
  Inject,
  Injectable,
  OnApplicationShutdown,
  OnModuleInit,
} from '@nestjs/common';
import { ClientKafka } from '@nestjs/microservices';
@Injectable()
export class AppService implements OnModuleInit, OnApplicationShutdown {
  constructor(
    @Inject('MAIN_SERVICE') private readonly stockService: ClientKafka,
  ) {}
  async onModuleInit() {
    const requestPatterns = ['buy'];
    requestPatterns.forEach((pattern) => {
      this.stockService.subscribeToResponseOf(pattern);
    });
    await this.stockService.connect();
  }
  async onApplicationShutdown() {
    await this.stockService.close();
  }
  async prepareStocks(data: any) {
    console.log('Prepare stocks');
    const stockname = data.stocks.split(',').map(function (n: any) {
      return String(n);
    });
    const price = data.trigger_prices.split(',').map(function (n: any) {
      return Number(n);
    });
    const stock = {};
    stockname.forEach((Curr_element: string, index: number) => {
      stock[Curr_element] = price[index];
    });
    console.log(stock);
    const result = await this.sendStocksToKafka(stock);
    return result;
  }
  async sendStocksToKafka(data: object) {
    return this.stockService.send('buy', data).subscribe((response) => {
      console.log(response);
    });
  }
}
