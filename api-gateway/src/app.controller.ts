import { Controller, Get } from '@nestjs/common';
import { Body, Header, Post, Res } from '@nestjs/common/decorators';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Post('/buy')
  @Header('Content-Type', 'application/json')
  buy(@Body() req, @Res() res) {
    console.log('Stock buy...');
    console.log(req);
    this.appService.prepareStocks(req);
    res.send(true);
  }
}
